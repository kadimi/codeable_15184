<?php 
	/*

	This template will generate this:

	<h3><a href="http://www.jpmmlab.com/csnetwork/member-location/sacramento-lead/">Sacramento Lead</a></h3>

	<p><small>City:</small> New York</p>

	<p>Lorem ipsum dolor sit amet, per dicit propriae percipit ad. Diam nihil nam ex, exerci utinam debitis no eos. Minimum vituperata nam id, an per possit nominavi. Nobis doctus nec id, vero molestiae est ea, labores nusquam eam et. Quot liber sensibus eos eu. Pri in paulo volutpat.</p>

	<p><small>Contact:</small> John Doe, <a href="mailto:john@.doecom">john@doe.com</a></p>

	<p><small>Website:</small> <a href="http://www.example.com">http://www.example.com</a></p>

	<p><a href="#" onclick="mapp0.getPoi(1).zoomIn(); return false;">Zoom</a></p>

	*/

	global $post;
	$postid = ($poi->postid) ? $poi->postid : $post->ID;
	$p = get_post( $postid );
	$m = get_post_meta( $postid );
?>

<h3><?php printf( '<a href="%s">%s</a>', get_permalink( $p ), $p->post_title ); ?></h3>

<p><?php printf( '%s', get_post_meta( $postid, 'wpcf-gm-city', true ) ) ?></p>

<p><?php echo wp_trim_words( $p->post_content, 20, '' ); ?></p>


<?php $email = get_post_meta( $postid, 'wpcf-gm-email', true ); ?>
<p><?php
	printf( '<small>Contact:</small> %s %s, %s'
		, get_post_meta( $postid, 'wpcf-gm-first-name', true )
		, get_post_meta( $postid, 'wpcf-gm-last-name', true )
		, sprintf( '<a href="mailto:%s">%s</a>', $email, $email )
	);
?></p>

<?php $website = get_post_meta( $postid, 'wpcf-gm-website', true ); ?>
<p><?php printf( '<a href="%s">%s</a>', $website, $website ); ?></p>

<p><?php echo $poi->get_links(); ?></p>
