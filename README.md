This is the solution for the task https://app.codeable.io/tasks/15184, it contains:

1. the "jpmmlab" plugin for changing colors.
2. The "map_poi.php" template for customizing the bubbles content, this file should be placed in the current theme.

